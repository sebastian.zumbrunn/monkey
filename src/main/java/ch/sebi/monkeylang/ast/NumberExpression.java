package ch.sebi.monkeylang.ast;

import ch.sebi.monkeylang.token.NumberToken;

public class NumberExpression implements Expression {
	private NumberToken token;

	public NumberExpression(NumberToken expression) {
		this.token = expression;
	}

	public NumberToken getToken() {
		return token;
	}
	
	@Override
	public String getSyntax() {
		return token.getLiteral();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(!getClass().equals(obj.getClass())) return false;
		NumberExpression other = (NumberExpression) obj;
		return token.equals(other.getToken());
	}
}
