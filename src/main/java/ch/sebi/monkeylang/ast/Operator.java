package ch.sebi.monkeylang.ast;

import static ch.sebi.monkeylang.ast.Precedence.*;
public enum Operator {
	PLUS("+", SUM),
	MINUS("-", SUM),
	MULTIPLY("*", PRODUCT),
	DIVIDE("/", PRODUCT)
	;
	private String literal;
	private int precedence;

	Operator(String literal, int precedence) {
		this.literal = literal;
		this.precedence = precedence;
	}

	public String getLiteral() {
		return literal;
	}

	public int getPrecedence() {
		return precedence;
	}
}
