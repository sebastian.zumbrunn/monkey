package ch.sebi.monkeylang.ast;

public class Precedence {
	public final static int LOWEST = 0;
	public final static int EQUALS = 1;
	public final static int LESSGREATER = 2;
	public final static int SUM = 3;
	public final static int PRODUCT = 4;
	public final static int PREFIX = 5;
	public final static int CALL = 6;
}
