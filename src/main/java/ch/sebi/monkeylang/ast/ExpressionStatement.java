package ch.sebi.monkeylang.ast;


public class ExpressionStatement implements Statement {
	private Expression expression;

	public ExpressionStatement(Expression expression) {
		this.expression = expression;
	}
	
	public Expression getExpression() {
		return expression;
	}

	@Override
	public String getSyntax() {
		return expression.getSyntax();
	}

	@Override
	public String toString() {
		return "ExpressionStatement { " + getSyntax() + " }";
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(!getClass().equals(obj.getClass())) return false;
		
		ExpressionStatement other = (ExpressionStatement) obj;
		return expression.equals(other.getExpression());
	}
}
