package ch.sebi.monkeylang.ast;

public class NullExpression implements Expression {

	@Override
	public String getSyntax() {
		return "<null>";
	}

}
