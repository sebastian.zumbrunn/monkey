package ch.sebi.monkeylang.ast;

import java.util.Arrays;
import java.util.stream.Collectors;

import ch.sebi.monkeylang.token.Token;

public class ProgramNode implements Statement {
	private Node[] nodes;

	public ProgramNode(Node[] nodes) {
		this.nodes = nodes;
	}

	public Node[] getNodes() {
		return nodes;
	}

	@Override
	public String getSyntax() {
		return Arrays.stream(nodes).filter(n -> n != null).map(Node::getSyntax).collect(Collectors.joining(" "));
	}

	@Override
	public String toString() {
		return "ProgramNode { " + getSyntax() + " }";
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(!getClass().equals(obj.getClass())) return false;
		ProgramNode other = (ProgramNode) obj;
		return Arrays.equals(other.getNodes(), getNodes());
	}
}
