package ch.sebi.monkeylang.ast;


public interface Node {
	/**
	 * Returns the code of the node 
	 * @return the code 
	 */
	public String getSyntax();
}
