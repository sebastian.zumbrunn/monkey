package ch.sebi.monkeylang.ast;

public class OperatorExpression implements Expression {
	private Operator op;
	private Expression leftExpr;
	private Expression rightExpr;

	public OperatorExpression(Operator op, Expression leftExpr, Expression rightExpr) {
		this.op = op;
		this.leftExpr = leftExpr;
		this.rightExpr = rightExpr;
	}

	public Operator getOp() {
		return op;
	}

	public Expression getLeftExpr() {
		return leftExpr;
	}

	public Expression getRightExpr() {
		return rightExpr;
	}

	@Override
	public String getSyntax() {
		return "(" + leftExpr.getSyntax() + " " + op.getLiteral() + " " + rightExpr.getSyntax() + ")";
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(!getClass().equals(obj.getClass())) return false;
		OperatorExpression other = (OperatorExpression) obj;
		return other.getOp() == op &&
				leftExpr.equals(other.getLeftExpr()) &&
				rightExpr.equals(other.getRightExpr());
	}
}
