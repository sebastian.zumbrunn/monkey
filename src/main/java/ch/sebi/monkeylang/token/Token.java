package ch.sebi.monkeylang.token;

/**
 * a token
 * @author sebi
 *
 */
public interface Token {
	/**
	 * Returns the literal value as a string of the token
	 * @return
	 */
	public String getLiteral(); 
}
