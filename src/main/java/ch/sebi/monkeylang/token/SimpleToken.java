package ch.sebi.monkeylang.token;

/**
 * A token simple token which doesn't have a dynamic value (like '=' or '+')
 * @author sebi
 *
 */
public enum SimpleToken implements Token{
	ILLEGAL("<ILLEGAL>"),
	EOF("<EOF>"),
	IDENT("<IDENT>"),
	
	ASSIGN("="),
	PLUS("+"),
	MINUS("-"),
	BANG("!"),
	ASTERISK("*"),
	SLASH("/"),
	
	STRING("\""),
	LT("<"),
	GT(">"),
	COMMA(","),
	LBRACE("{"),
	RBRACE("}"),
	LPAREN("("),
	RPAREN(")"),
	SEMICOLON(";")
	;

	private String literal;

	SimpleToken(String literal) {
		this.literal = literal;
	}

	public String getLiteral() {
		return literal;
	}
	
	@Override
	public String toString() {
		return "SimpleToken { " + getLiteral() + "}";
	}
}
