package ch.sebi.monkeylang.token;

public class NumberToken implements Token {
	private double number;
	

	public NumberToken(String literal) {
		this.number = Double.parseDouble(literal);
	}

	public int getAsInt() {
		return (int) number;
	}
	
	public double getAsDouble() {
		return number;
	}

	@Override
	public String getLiteral() {
		return String.valueOf(number);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if (!getClass().equals(obj.getClass()))
			return false;
		NumberToken other = (NumberToken) obj;
		return number == other.getAsDouble();
	}
	
	@Override
	public String toString() {
		return String.format("NumberToken {number = \"%s\"}", getLiteral()); 
	}
}
