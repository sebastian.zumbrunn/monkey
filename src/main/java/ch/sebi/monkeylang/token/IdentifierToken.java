package ch.sebi.monkeylang.token;

/**
 * An identifier token
 * 
 * @author sebi
 *
 */
public class IdentifierToken implements Token {
	/**
	 * the literal value
	 */
	private String literal;

	/**
	 * Constructor
	 * 
	 * @param literal the literal value
	 */
	public IdentifierToken(String literal) {
		this.literal = literal;
	}

	@Override
	public String getLiteral() {
		return literal;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if (!getClass().equals(obj.getClass()))
			return false;
		IdentifierToken other = (IdentifierToken) obj;
		return (literal != null && literal.equals(other.getLiteral()))
				|| (literal == null && other.getLiteral() == null);
	}
	
	@Override
	public String toString() {
		return String.format("IdentifierToken {identifier = \"%s\"}", getLiteral()); 
	}

}
