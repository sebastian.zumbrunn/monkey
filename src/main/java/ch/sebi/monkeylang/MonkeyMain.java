package ch.sebi.monkeylang;

import java.io.IOException;

import ch.sebi.monkeylang.repl.REPL;

public class MonkeyMain {

	public static void main(String[] args) throws IOException {
		new REPL(System.in, System.out).start();
	}
}
