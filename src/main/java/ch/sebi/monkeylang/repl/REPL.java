package ch.sebi.monkeylang.repl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;

import ch.sebi.monkeylang.lexer.Lexer;
import ch.sebi.monkeylang.token.SimpleToken;
import ch.sebi.monkeylang.token.Token;

public class REPL {
	private InputStream in;
	private OutputStream out;
	private BufferedReader reader;
	private PrintWriter writer;

	public REPL(InputStream in, OutputStream out) {
		this.in = in;
		this.out = out;
		reader = new BufferedReader(new InputStreamReader(in));
		writer = new PrintWriter(out, true);
	}
	
	public void start() throws IOException {
		writer.println("Welcome to Monkey");
		String input;
		writer.printf("> ");
		while((input = reader.readLine()) != null) {
			Lexer lexer = new Lexer(input);
			Token t;
			while((t = lexer.getNextToken()) != SimpleToken.EOF) {
				writer.print(t.getLiteral());
			}
			writer.printf("\n> ");
		}
	}
	
}
