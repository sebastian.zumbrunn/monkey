package ch.sebi.monkeylang.exception;

import ch.sebi.monkeylang.ast.Expression;
import ch.sebi.monkeylang.ast.Node;
import ch.sebi.monkeylang.token.Token;

public class NoInfixExpressionFound extends ParseException {

	public NoInfixExpressionFound(Token n) {
		super("No infix operator for token \"" + n.getClass().getName() + "\" was found");
	}

	
}
