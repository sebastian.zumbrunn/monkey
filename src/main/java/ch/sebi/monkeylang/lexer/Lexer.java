package ch.sebi.monkeylang.lexer;

import java.io.EOFException;

import ch.sebi.monkeylang.token.IdentifierToken;
import ch.sebi.monkeylang.token.NumberToken;
import ch.sebi.monkeylang.token.SimpleToken;
import ch.sebi.monkeylang.token.Token;

public class Lexer {
	public final static char[] WHITESPACE_CHARACTERS = new char[] { ' ' };
	private String input;
	private int currentIndex;
	private char currentChar;

	public Lexer(String input) {
		this.input = input;
		this.currentIndex = 0;
	}

	public Token getNextToken() {
		try {
			skipWhitespaces();
		} catch (EOFException e1) {
			return SimpleToken.EOF;
		}
		char currentChar;
		try {
			currentChar = readChar();
		} catch (EOFException e) {
			return SimpleToken.EOF;
		}
		switch (currentChar) {
		case '=':
			return SimpleToken.ASSIGN;
		case '+':
			return SimpleToken.PLUS;
		case '-':
			return SimpleToken.MINUS;
		case '*':
			return SimpleToken.ASTERISK;
		case '/':
			return SimpleToken.SLASH;
		case '!':
			return SimpleToken.BANG;
		case '"':
			return SimpleToken.STRING;
		case '>':
			return SimpleToken.GT;
		case '<':
			return SimpleToken.LT;
		case ',':
			return SimpleToken.COMMA;
		case '{':
			return SimpleToken.LBRACE;
		case '}':
			return SimpleToken.RBRACE; 
		case '(':
			return SimpleToken.LPAREN;
		case ')':
			return SimpleToken.RPAREN;
		default:
			if (isLetter(currentChar)) {
				return readIdentifierToken();
			} else if (isNumber(currentChar)) {
				return readNumberToken();
			}
			return SimpleToken.ILLEGAL;
		}
	}

	private Token readIdentifierToken() {
		String literal = "";
		try {
			literal += "" + currentChar;
			while (isLetter(peekChar()) || isNumber(peekChar())) {
				literal += readChar();
			}
		} catch (EOFException e) {
			if (literal.equals("")) {
				return SimpleToken.EOF;
			}
		}
		return new IdentifierToken(literal);
	}

	private Token readNumberToken() {
		String literal = "" + currentChar;
		try {
			while (isNumber(peekChar())) {
				literal += readChar();
			}
		} catch (EOFException e) {
			if (literal.equals("")) {
				return SimpleToken.EOF;
			}
		}
		return new NumberToken(literal);
	}
	
	private void skipWhitespaces() throws EOFException {
		while(isWhitespace(peekChar())) {
			readChar(); //advances the index
		}
	}

	private char readChar() throws EOFException {
		if (currentIndex >= input.length()) {
			throw new EOFException();
		}
		currentChar =  input.charAt(currentIndex++);
		return currentChar;
	}

	private char peekChar() throws EOFException {
		if (currentIndex >= input.length()) {
			throw new EOFException();
		}
		currentChar =  input.charAt(currentIndex);
		return currentChar;
	}

	private boolean isNumber(char c) {
		return c >= '0' && c <= '9';
	}

	private boolean isLetter(char c) {
		return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c == '_');
	}
	
	private boolean isWhitespace(char c) {
		for(char whitespaceChar : WHITESPACE_CHARACTERS) {
			if(whitespaceChar == c) return true;
		}
		return false;
	}
}
