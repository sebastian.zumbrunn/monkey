package ch.sebi.monkeylang.parser;

import java.util.ArrayList;
import java.util.List;

import ch.sebi.monkeylang.ast.Expression;
import ch.sebi.monkeylang.ast.ExpressionStatement;
import ch.sebi.monkeylang.ast.Node;
import ch.sebi.monkeylang.ast.NullExpression;
import ch.sebi.monkeylang.ast.NumberExpression;
import ch.sebi.monkeylang.ast.Operator;
import ch.sebi.monkeylang.ast.OperatorExpression;
import static ch.sebi.monkeylang.ast.Precedence.*;
import ch.sebi.monkeylang.ast.ProgramNode;
import ch.sebi.monkeylang.ast.Statement;
import ch.sebi.monkeylang.exception.NoInfixExpressionFound;
import ch.sebi.monkeylang.exception.ParseException;
import ch.sebi.monkeylang.lexer.Lexer;
import ch.sebi.monkeylang.token.NumberToken;
import ch.sebi.monkeylang.token.SimpleToken;
import ch.sebi.monkeylang.token.Token;

public class Parser {
	private Lexer lexer;
	private Token currentToken;
	private Token peekToken;

	public Parser(Lexer lexer) {
		this.lexer = lexer;
		nextToken();
		nextToken();
	}

	private void nextToken() {
		currentToken = peekToken;
		peekToken = lexer.getNextToken();
	}

	private boolean currentTokenIs(Token t) {
		return currentToken.equals(t);
	}

	private boolean currentTokenIs(Class<?> tokenClass) {
		return currentToken.getClass().equals(tokenClass);
	}

	private boolean peekTokenIs(Token... tokens) {
		for (Token t : tokens) {
			if (peekToken.equals(t)) {
				return true;
			}
		}
		return false;
	}

	private boolean peekTokenIs(Class<?>... tokenClasses) {
		for (Class<?> c : tokenClasses) {
			if (peekToken.getClass().equals(c)) {
				return true;
			}
		}
		return false;
	}

	private boolean expectPeek(Token t) {
		if (peekTokenIs(t)) {
			nextToken();
			return true;
		}
		// TODO: add error
		return false;
	}

	private boolean peekTokenIsInfix() {
		return peekTokenIs(SimpleToken.PLUS, SimpleToken.MINUS, SimpleToken.ASTERISK, SimpleToken.SLASH);
	}

	public Node parse() {
		return parseProgram();
	}

	private Node parseProgram() {
		List<Node> nodes = new ArrayList<>();
		while (!currentTokenIs(SimpleToken.EOF)) {
			Statement stmt = parseStatement();
			if (stmt == null)
				break;
			nodes.add(stmt);
			nextToken();
		}
		return new ProgramNode(nodes.toArray(new Node[nodes.size()]));
	}

	private Statement parseStatement() {
		return parseExpressionStatement();
	}

	private ExpressionStatement parseExpressionStatement() {
		Expression expr = parseExpression(LOWEST);
		return new ExpressionStatement(expr);
	}

	private Expression parseExpression(int precedence) {
		Expression leftExpr = parsePrefixExpression();
		while(!peekTokenIs(SimpleToken.SEMICOLON) && precedence < peekPrecedence()) {
			if(peekTokenIsInfix()) {
				nextToken();
				leftExpr = parseInfixExpression(leftExpr);
			}
		}
		return leftExpr;
	}

	private int peekPrecedence() {
		if(peekTokenIs(SimpleToken.PLUS, SimpleToken.MINUS)) return SUM;
		if(peekTokenIs(SimpleToken.ASTERISK, SimpleToken.SLASH)) return PRODUCT;
		return LOWEST;
	}

	private Expression parsePrefixExpression() {
		if (currentTokenIs(NumberToken.class)) {
			return parseNumber();
		}
		//TODO: add error handling
		return new NullExpression();
	}

	private NumberExpression parseNumber() {
		NumberToken token = (NumberToken) currentToken;
		return new NumberExpression(token);
	}


	private Expression parseInfixExpression(Expression leftExpr) {
		if(currentTokenIs(SimpleToken.PLUS)) {
			return parseOperatorExpression(leftExpr, Operator.PLUS);
		} 
		if(currentTokenIs(SimpleToken.MINUS)) {
			return parseOperatorExpression(leftExpr, Operator.MINUS);
		}
		if(currentTokenIs(SimpleToken.ASTERISK)) {
			return parseOperatorExpression(leftExpr, Operator.MULTIPLY);
		}
		if(currentTokenIs(SimpleToken.SLASH)) {
			return parseOperatorExpression(leftExpr, Operator.DIVIDE);
		}
		//TODO: add error handling
		return new NullExpression();
	}

	private Expression parseOperatorExpression(Expression leftExpr, Operator op) {
		nextToken();
		Expression rightExpr = parseExpression(op.getPrecedence());
		return new OperatorExpression(op, leftExpr, rightExpr);
	}

}
