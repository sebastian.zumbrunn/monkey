package ch.sebi.monkeylang.parser;

import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import ch.sebi.monkeylang.ast.Expression;
import ch.sebi.monkeylang.ast.ExpressionStatement;
import ch.sebi.monkeylang.ast.Node;
import ch.sebi.monkeylang.ast.NumberExpression;
import ch.sebi.monkeylang.ast.Operator;
import ch.sebi.monkeylang.ast.OperatorExpression;
import ch.sebi.monkeylang.ast.ProgramNode;
import ch.sebi.monkeylang.ast.Statement;
import ch.sebi.monkeylang.lexer.Lexer;
import ch.sebi.monkeylang.token.NumberToken;

public class ParserTest {
	private static Stream<Arguments> parserTestArguments() {
		return Stream.of(Arguments.of("12",
				programNodeOf(exprStmt(numberExpr(12)))),
				Arguments.of("12 + 4", programNodeOf(exprStmt(opExpr(numberExpr(12), Operator.PLUS, numberExpr(4))))),
				Arguments.of("12 + 4 - 2", 
						programNodeOf(
							exprStmt(
									opExpr(
										opExpr(numberExpr(12), Operator.PLUS, numberExpr(4)),
										Operator.MINUS,
										numberExpr(2)
									)
							)
						)),
				Arguments.of("12 + 4 * 2", 
						programNodeOf(
							exprStmt(
									opExpr(
										numberExpr(12),
										Operator.PLUS,
										opExpr(numberExpr(4), Operator.MULTIPLY, numberExpr(2))
									)
							)
						)),
				Arguments.of("12 / 4 * 2", 
						programNodeOf(
							exprStmt(
									opExpr(
										opExpr(numberExpr(12), Operator.DIVIDE, numberExpr(4)),
										Operator.MULTIPLY,
										numberExpr(2)
									)
							)
						))
				);
	}

	@ParameterizedTest
	@MethodSource("parserTestArguments")
	public void testParser(String input, Node expected) {
		Lexer lexer = new Lexer(input);
		Parser parser = new Parser(lexer);
		Node got = parser.parse();
		Assert.assertEquals(expected, got);
	}

	private static ProgramNode programNodeOf(Node... nodes) {
		return new ProgramNode(nodes);
	}

	private static ExpressionStatement exprStmt(Expression expr) {
		return new ExpressionStatement(expr);
	}

	private static NumberExpression numberExpr(double number) {
		return new NumberExpression(new NumberToken(String.valueOf(number)));
	}

	private static Expression opExpr(Expression leftExpr, Operator op, Expression rightExpr) {
		return new OperatorExpression(op, leftExpr, rightExpr);
	}
}
