package ch.sebi.monkeylang.lexer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import ch.sebi.monkeylang.token.IdentifierToken;
import ch.sebi.monkeylang.token.NumberToken;
import ch.sebi.monkeylang.token.SimpleToken;
import ch.sebi.monkeylang.token.Token;

public class LexerTest {

	private static Stream<Arguments> lexerTestArguments() {
		return Stream.of(
				Arguments.of("+", new Token[] {SimpleToken.PLUS}),
				Arguments.of("-", new Token[] {SimpleToken.MINUS}),
				Arguments.of("hello", new Token[] {new IdentifierToken("hello")}),
				Arguments.of("12", new Token[] {new NumberToken("12")}),
				Arguments.of("12+ 20", new Token[] {new NumberToken("12"), SimpleToken.PLUS, new NumberToken("20")}),
				Arguments.of("hello+-/", new Token[] {new IdentifierToken("hello"), 
						SimpleToken.PLUS, SimpleToken.MINUS, SimpleToken.SLASH}),
				Arguments.of("1234 Hello + -", new Token[] {new NumberToken("1234"), new IdentifierToken("Hello"), SimpleToken.PLUS, SimpleToken.MINUS}),
				Arguments.of("hello_test123", new Token[] {new IdentifierToken("hello_test123")}));
	}

	@ParameterizedTest(name = "run #{index} with [{arguments}]")
	@MethodSource("lexerTestArguments")
	void testLexing(String input, Token[] expected) {
		Lexer lexer = new Lexer(input);
		List<Token> tokens = new ArrayList<>();
		Token token = null;
		while ((token = lexer.getNextToken()) != SimpleToken.EOF) {
			tokens.add(token);
		}
		Token[] got = tokens.toArray(new Token[tokens.size()]);
		Assert.assertArrayEquals(expected, got);
	}
}
